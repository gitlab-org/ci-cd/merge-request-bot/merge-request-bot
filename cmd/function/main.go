package main

import (
	"log"
	"net/http"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/ci-cd/merge-request-bot/merge-request-bot/internal/function"
	"gitlab.com/gitlab-org/ci-cd/merge-request-bot/merge-request-bot/internal/gitlab"
)

func main() {
	logger := logrus.New()
	logger.SetFormatter(&logrus.JSONFormatter{})
	entry := logger.WithFields(logrus.Fields{
		"function": "merge-request-bot",
	})

	client := gitlab.NewClient(os.Getenv("GITLAB_GRAPHQL"), os.Getenv("GITLAB_PAT"), entry)

	fn := function.NewFunction(*client, entry, function.WithTokenFromEnv())
	http.HandleFunc("/", fn.Handler)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
