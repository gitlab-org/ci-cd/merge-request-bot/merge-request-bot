package gitlab

import (
	"context"

	"github.com/machinebox/graphql"
)

const (
	gitlabOrgName = "gitlab-org"
)

type membershipPayload struct {
	Group struct {
		GroupMembers struct {
			Edges []struct {
				Node struct {
					ID string `json:"id"`
				} `json:"node"`
			} `json:"edges"`
		} `json:"groupMembers"`
	} `json:"group"`
}

func (c *Client) GetTeamMembership(ctx context.Context, username string) (bool, error) {
	logger := c.logger.WithField("username", username)
	logger.Info("Querying gitlab org membership")

	req := graphql.NewRequest(`
query Memberships($username: String, $group: ID!) {
	group(fullPath: $group) {
		groupMembers(search: $username) {
			edges {
				node {
					id
				}
			}
		}
	}
}
`)

	req.Var("username", username)
	req.Var("group", gitlabOrgName)
	req.Header.Add("Authorization", "Bearer "+c.token)

	var resp membershipPayload
	if err := c.graphql.Run(ctx, req, &resp); err != nil {
		return false, err
	}

	if len(resp.Group.GroupMembers.Edges) > 0 {
		return true, nil
	}

	return false, nil
}
