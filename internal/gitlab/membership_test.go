package gitlab

import (
	"context"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/require"
)

const isMember = `
{
  "data": {
    "group": {
      "groupMembers": {
        "edges": [
          {
            "node": {
              "id": "gid://gitlab/GroupMember/9207807"
            }
          }
        ]
      }
    }
  }
}`

const isNotMember = `
{
  "data": {
    "group": {
      "groupMembers": {
        "edges": []
      }
    }
  }
}`

func TestClient_GetTeamMembership(t *testing.T) {
	tests := []struct {
		name         string
		resp         string
		wantIsMember bool
		wantErr      bool
	}{
		{
			name:         "is member",
			resp:         isMember,
			wantIsMember: true,
			wantErr:      false,
		},
		{
			name:         "not member",
			resp:         isNotMember,
			wantIsMember: false,
			wantErr:      false,
		},
		{
			name:         "bogus response",
			resp:         "",
			wantIsMember: false,
			wantErr:      true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				defer r.Body.Close()

				w.Header().Set("Content-Type", "application/json")
				_, err := fmt.Fprint(w, tt.resp)
				require.NoError(t, err)
			}))

			c := NewClient(srv.URL, "token", nil)
			gotIsMember, err := c.GetTeamMembership(context.Background(), "username")
			if !tt.wantErr {
				require.NoError(t, err)
			}

			require.Equal(t, tt.wantIsMember, gotIsMember)
		})
	}
}
