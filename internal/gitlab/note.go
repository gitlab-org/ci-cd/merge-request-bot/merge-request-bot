package gitlab

import (
	"context"
	"fmt"

	"github.com/machinebox/graphql"
)

//nolint:lll
const noteBody = `
Thank you for your contribution! :tada:

We strive to make the contribution experience as smooth as possible.

Some contributions require several iterations of review and we try to mentor contributors during this process.
However, we understand that some reviews can be very time consuming.
If you would prefer us to continue the work you've submitted now or at any point in the future please let us know.

If you're okay with being part of our review process (and we hope you are!), there are several initial checks we ask you to make:
* The merge request description clearly explains:
  - The problem being solved.
  - The best way a reviewer can test your changes (is it possible to provide an example?).
* If the pipeline failed, do you need help identifying what failed?
* Check that Go code follows our [Go
  guidelines](https://docs.gitlab.com/ee/development/go_guide/index.html#code-review).
* Read our [contributing to GitLab
  Runner](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/CONTRIBUTING.md#contribute-to-gitlab-runner) document.

---

We sometimes get a large number of community contributions and have to
[prioritize reviews](https://gitlab.com/gitlab-org/gitlab-runner/-/blob/master/CONTRIBUTING.md#how-we-prioritize-mrs-from-the-wider-community).`

type createNotePayload struct {
	CreateNote struct {
		Note struct {
			ID     string `json:"id"`
			Author struct {
				Username string `json:"username"`
			} `json:"author"`
		} `json:"note"`
	} `json:"createNote"`
}

// CreateMergeRequestNote creates a note (comment) on the specified merge
// request. Any error returned from the API is retuned.
func (c *Client) CreateMergeRequestNote(ctx context.Context, id int) error {
	logger := c.logger.WithField("merge_request_id", id)

	req := graphql.NewRequest(`
mutation ($id: ID! $body: String!) {
  createNote(input: {noteableId: $id, body: $body}) {
    note {
      id
    }
  }
}
`)

	req.Var("id", fmt.Sprintf("gid://gitlab/MergeRequest/%d", id))
	req.Var("body", noteBody)
	req.Header.Add("Authorization", "Bearer "+c.token)

	var resp createNotePayload
	if err := c.graphql.Run(ctx, req, &resp); err != nil {
		return err
	}

	logger.Info("Created merge request note")

	return nil
}
