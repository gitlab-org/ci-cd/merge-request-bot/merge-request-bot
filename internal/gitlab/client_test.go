package gitlab

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/machinebox/graphql"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestNewClient(t *testing.T) {
	const token = "TOKEN"

	const expectedBody = `{"query":"{ currentUser { username } }","variables":null}
`

	logger := logrus.New()
	logger.SetLevel(logrus.DebugLevel)

	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer r.Body.Close()
		b, err := ioutil.ReadAll(r.Body)
		require.NoError(t, err)
		assert.Equal(t, expectedBody, string(b))

		w.Header().Set("Content-Type", "application/json")
		_, err = fmt.Fprint(w, `{ "data": { "currentUser": { "username": "username" } } }`)
		require.NoError(t, err)
	}))

	c := NewClient(srv.URL, token, logger)

	req := graphql.NewRequest("{ currentUser { username } }")
	resp := struct {
		CurrentUser struct {
			Username string `json:"username"`
		} `json:"currentUser"`
	}{}

	err := c.graphql.Run(context.Background(), req, &resp)
	require.NoError(t, err)
	assert.Equal(t, resp.CurrentUser.Username, "username")
}

func TestNewClient_NullLogger(t *testing.T) {
	c := NewClient("https://gitlab.com/api/graphql", "token", nil)
	assert.NotNil(t, c.logger)
}
