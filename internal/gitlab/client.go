package gitlab

import (
	"github.com/machinebox/graphql"
	"github.com/sirupsen/logrus"
)

// Client is used to communicate with GitLab's API.
type Client struct {
	graphql graphql.Client
	token   string
	logger  logrus.FieldLogger
}

// NewClient creates a new client for the specified endpoint using the token for
// authorization. The token is personal access token that should have `api` as
// permissions, as explained in
// https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html
func NewClient(api string, token string, logger logrus.FieldLogger) *Client {
	if logger == nil {
		newLogger := logrus.New()
		newLogger.SetFormatter(&logrus.JSONFormatter{})
		logger = newLogger
	}

	client := graphql.NewClient(api)
	client.Log = func(s string) {
		logger.Debug(s)
	}

	return &Client{
		graphql: *client,
		token:   token,
		logger:  logger,
	}
}
