⚠️ Deprecated in favor of https://gitlab.com/gitlab-org/quality/triage-serverless ⚠️

# Merge Request Bot

Post a comment as soon as a merge request is opened by a community member that
is not a GitLab team member indicating what are the next steps.

![merge request bot demo](https://i.imgur.com/DjQ3oWs.png)

## Deploy

This project automatically deploys to
a [CloudRun](https://cloud.google.com/run/) service through CI. The service is
publicly available so anything can send requests to it. The service is defined
in
[infrastructure/cloud-run](https://gitlab.com/merge-request-bot/infrastructure/cloud-run).

For a successful deployment the project requires the following
[variables](https://docs.gitlab.com/ee/ci/variables)
- `CLOUD_RUN_REGION`: The region to deploy too for example
  `europe-west4`
- `GCP_CLOUD_BUILD_LOGS_DIR`: The path of the GCS bucket for [cloud
  build](https://cloud.google.com/cloud-build/) logs, for example
  `gs://merge-request-bot-cloud-build-3104b86/logs`
- `GCP_PROJECT_ID`: The ID of the project where the cloud run service is
  created in.
- `GCP_SERVICE_ACCOUNT`: A [file
  based](https://docs.gitlab.com/ee/ci/variables/#custom-environment-variables-of-type-file)
  variable which is the [service
  account](https://cloud.google.com/compute/docs/access/service-accounts)
  created by the [infrastructure/cloud-run](https://gitlab.com/merge-request-bot/infrastructure/cloud-run).

## Configure

This should be configured as a [GitLab
webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)
for the [merge request event](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#merge-request-events).
This webhook listens for all the merge request events but only post
comments when a new merge request by a community member is created.

![webhook configuration example](https://i.imgur.com/6Ly3Uqx.png)

1. Set `URL` to the value of the cloud run service shown in GCP console.
1. Set the `Secret Token` to be the same value as
`GITLAB_WEBHOOK_SECRET_TOKEN` set in
[infrastructure/cloud-run](https://gitlab.com/merge-request-bot/infrastructure/cloud-run).
1. Tick `Merge request events`.

## Development

You can run the function locally by running the command below where `GITLAB_PAT`
is a [GitLab PAT
token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) for GitLab.com.

```shell
GITLAB_PAT="....." GITLAB_WEBHOOK_SECRET_TOKEN=foo GITLAB_GRAPHQL=https://gitlab.com/api/graphql go run ./cmd/function
```

Sending example requests to merge-request-bot:

1. New merge request from GitLab team member.
    ```shell
    curl -H "X-Gitlab-Event: Merge Request Hook" -H "X-Gitlab-Token: foo" localhost:8080 -d @internal/function/testdata/open.json
    ```

1. New merge request from wider community.
    ```shell
    curl -H "X-Gitlab-Event: Merge Request Hook" -H "X-Gitlab-Token: foo" localhost:8080 -d @internal/function/testdata/open_community_contribution.json
    ```

